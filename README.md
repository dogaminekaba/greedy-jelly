# Description

Play as a greedy yellow Jelly and try to eat as many small Jellies as you can!! 

Be careful about bigger Jellies though, they can swallow you up :(

# Screenshots

![GreedyJelly_start](https://bitbucket.org/dogaminekaba/greedy-jelly/downloads/GreedyJelly_start.png)

![GreedyJelly_start](https://bitbucket.org/dogaminekaba/greedy-jelly/downloads/GreedyJelly_play.png)

![GreedyJelly_start](https://bitbucket.org/dogaminekaba/greedy-jelly/downloads/GreedyJelly_score.png)