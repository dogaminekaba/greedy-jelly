package objects
{
  import nape.geom.Vec2;
  import nape.geom.Vec2List;
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  import nape.space.Space;
  
  import starling.display.MovieClip;
  import starling.display.Sprite;
  
  import utilities.GameProperties;
  
  public class Jelly extends Sprite
  {
    public var body:Body;
    public var type:int;
    public var size:Number;
    private var isMyJelly:Boolean;
    private var scaleCo:Number;
    private var jellyImage:MovieClip;
    private var space:Space;
    private var speed:Number;
    private var bodyBeforeScale:Number;
    
    public function Jelly(space:Space, scaleCo:Number, speed:Number, isMyJelly:Boolean=false)
    {
      super();
      this.space = space;
      this.scaleCo = scaleCo;
      this.speed = speed;
      this.isMyJelly = isMyJelly;
      
      size = scaleCo;
      
      bodyBeforeScale = 1/scaleCo;
      createBody();
    }
    
    private function drawBody():void
    {
      type = defineType();
      
      switch(type)
      {
        //my jelly
        case 0:
          jellyImage = new MovieClip(Assets.getAtlas().getTextures("my_jelly"));
          break;
        //small jelly
        case 1:
          jellyImage = new MovieClip(Assets.getAtlas().getTextures("small_jelly"));
          break;
        //big jelly
        case 2:
          jellyImage = new MovieClip(Assets.getAtlas().getTextures("big_jelly"));
          break;
        default:
          break;
      }
      
      //scaling for body structure and display match
      jellyImage.scaleX = scaleCo;
      jellyImage.scaleY = scaleCo;
      
      jellyImage.x = 0;
      jellyImage.y = 0;
      
      addChild(jellyImage);
      body.scaleShapes(scaleCo,scaleCo);
      
      this.x = body.position.x;
      this.y = body.position.y;
      
    }
    
    private function defineType():int
    {
      if(isMyJelly)
        type = 0;
      else if(size <= GameProperties.playerSize)
        type = 1;
      else
        type = 2;
      
      return type;
    }
    private function createBody():void
    {
      body = new Body(BodyType.KINEMATIC, new Vec2(0,0));
      
      var pointList:Vec2List = new Vec2List;
      
      //jelly body
      pointList.add(new Vec2(30,55));
      pointList.add(new Vec2(110,40));
      pointList.add(new Vec2(195,55));
      pointList.add(new Vec2(225,250));
      pointList.add(new Vec2(0,255));
      
      body.shapes.add(new Polygon(pointList));
      body.space = space;
      
      drawBody();
    }
    public function update():void
    {
      body.position.x += speed;
      this.x = body.position.x;
      this.y = body.position.y;
      
      if(size < GameProperties.playerSize)
        changeToSmall();
      
    }
    public function scaleJelly(scaleCo:Number):void
    {
      size = scaleCo;
      
      jellyImage.scaleX = scaleCo;
      jellyImage.scaleY = scaleCo;
      
      body.scaleShapes(bodyBeforeScale,bodyBeforeScale);
      body.scaleShapes(scaleCo,scaleCo);
      bodyBeforeScale = 1/scaleCo;
    }
    public function changeToSmall():void
    {
      removeChild(jellyImage);
      jellyImage = new MovieClip(Assets.getAtlas().getTextures("small_jelly"));
      
      jellyImage.scaleX = scaleCo;
      jellyImage.scaleY = scaleCo;
      
      jellyImage.x = 0;
      jellyImage.y = 0;
      
      addChild(jellyImage);
      
      type = 1;
    }
  }
}