package
{
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.events.MouseEvent;
  
  import screens.InGame;
  import screens.Welcome;
  
  import starling.core.Starling;
  
  [SWF(frameRate="60", width="500", height="500", backgroundColor="0xffffff")]
  public class GreedyJelly extends Sprite
  {
    private static var _starling:Starling;
    public function GreedyJelly()
    {
      this.addEventListener(Event.ADDED_TO_STAGE,onAddedtoStage);
    }
    
    protected function onAddedtoStage(event:Event):void
    {
      this.removeEventListener(Event.ADDED_TO_STAGE,onAddedtoStage);
      _starling = new Starling(Welcome, stage);
      _starling.start();
      stage.addEventListener(MouseEvent.RIGHT_CLICK, onRightClick);
      stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
    }
    
    protected function onMouseMove(event:MouseEvent):void
    {
      if (InGame.myJelly != null)
      {
        InGame.myJelly.body.position.x = event.localX - InGame.myJelly.width/2;
        InGame.myJelly.body.position.y = event.localY - InGame.myJelly.height/2;
      }
    }
    
    protected function onRightClick(event:MouseEvent):void
    {
      //Intentionally left blank
    }
  }
}