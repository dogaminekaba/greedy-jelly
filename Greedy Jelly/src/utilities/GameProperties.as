package utilities
{
  public class GameProperties
  {
    public static var playerSize:Number;
    
    public static const speedCo:Number = 0.7;
    
    public static const myJellyScaleCo:Number = 0.001;
    public static const myJellySize:Number = 0.1;
    
    public static const stageWidth:int = 500;
    public static const stageHeight:int = 500;
    
    //Welcome screen
    public static const gameTitleX:int = 30;
    public static const gameTitleY:int = 50;
    
    public static const jelliesImageX:int = 70;
    public static const jelliesImageY:int = 146;
    
    public static const playButtonX:int = 80;
    public static const playButtonY:int = 350;
    
    public static const infoButtonX:int = 320;
    public static const infoButtonY:int = 340;
    
    //InGame screen
    public static const scoreBoardX:int = -20;
    public static const scoreBoardY:int = 10;
    
    public static const overlayX:int = 10;
    public static const overlayY:int = 10;
    
    //GameOver
    public static const playAgainButtonY:int = 350;
    public static const menuJellyY:int = 150;
    public static const scoreTitleY:int = 50;
    public static const gameoverScoreY:int = 180;
    
  }
}