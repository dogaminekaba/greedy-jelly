package screens 
{
  import flash.ui.Mouse;
  
  import nape.geom.Vec2;
  import nape.space.Space;
  
  import objects.Jelly;
  
  import starling.display.Button;
  import starling.display.Image;
  import starling.display.Sprite;
  import starling.events.Event;
  import starling.text.TextField;
  import starling.utils.HAlign;
  import starling.utils.VAlign;
  
  import utilities.GameProperties;
  import utilities.GameUtilities;
  
  public class InGame extends Sprite
  {
    public static var myJelly:Jelly;
    private var space:Space;
    private var jellyRightCreated:Boolean = false;
    private var jellyLeftCreated:Boolean = false;
    private var createdJellies:Array;
    private var scaleCo:Number;
    private var score:int;
    private var scoreBoard:TextField;
    private var bgImage:Image;
    
    public function InGame()
    {
      super();
      addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
    }
    
    private function onAddedToStage(e:Event):void
    {
      removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
      setUp();
    }
    
    private function setUp():void
    {
      space = new Space(new Vec2(0,0));
      createdJellies = new Array();
      
      score = 0;
      
      //add background image
      bgImage = new Image(Assets.getAtlas().getTexture("bg"));
      bgImage.alpha = 0.8;
      addChild(bgImage);
      
      //create my jelly
      scaleCo = GameProperties.myJellySize;
      myJelly = new Jelly(space, scaleCo, 0, true);
      myJelly.body.position.x = (GameProperties.stageWidth - myJelly.width)/2;
      myJelly.body.position.y = (GameProperties.stageHeight - myJelly.height)/2;
      createdJellies.push(myJelly);
      addChild(myJelly);
      
      //set player size
      GameProperties.playerSize = myJelly.size;
      
      createScoreBoard();
      
      Mouse.hide();
      
      addEventListener(Event.ENTER_FRAME, onEnterFrame);
    }
    
    private function onEnterFrame():void
    {
      createJellies();
      updateJellies();
    }
    
    private function updateJellies():void
    {
      var index:int;
      
      //update positions
      for (var i:int = 0; i < createdJellies.length; i++) 
        createdJellies[i].update();
      
      //check collision with my jelly
      for (i = 0; i < createdJellies.length; i++) 
      {
        if (createdJellies[i] != myJelly && nape.geom.Geom.intersectsBody(myJelly.body, createdJellies[i].body)) 
        {
          //if it's small jelly
          if (createdJellies[i].type == 1) 
          {
            //update score
            score += createdJellies[i].size*100;
            scoreBoard.text = score.toString();
            
            //remove small jelly
            space.bodies.remove(createdJellies[i].body);
            removeChild(createdJellies[i]);
            createdJellies.splice(i, 1);
            
            //scale my jelly
            scaleCo+=GameProperties.myJellyScaleCo;
            myJelly.scaleJelly(scaleCo);
            
            //update player size
            GameProperties.playerSize = myJelly.size;
            
          }
          //if it's big jelly
          //Game Over
          else if(createdJellies[i].type == 2)
          {
            gameOver();
            removeEventListener(Event.ENTER_FRAME, onEnterFrame);
            break;
          }
        }
        //if jellies are out of stage
        else if (createdJellies[i].x > GameProperties.stageWidth || createdJellies[i].x < -GameProperties.stageWidth/3) 
        {
          removeChild(createdJellies[i]);
          space.bodies.remove(createdJellies[i].body);
          createdJellies.splice(i,1);
        }
      }
    }
    
    private function createJellies():void
    {
      var date:Date = new Date();
      var miliseconds:int = date.getUTCMilliseconds()/100;
      var jelly:Jelly;
      var speed:Number;
      var type:int;
      var size:Number;
      
      //create jelly from right side
      if ((miliseconds == 0 || miliseconds == 1) && !jellyRightCreated) 
      {
        speed = Math.random() + GameProperties.speedCo;
        size = myJelly.size * (Math.random()+Math.random());
        
        jelly = new Jelly(space, size, speed);
        
        //set position
        jelly.body.position.x = -jelly.width;
        jelly.body.position.y = GameUtilities.randomInt(0, GameProperties.stageHeight - jelly.height);
        
        createdJellies.push(jelly);
        addChild(jelly);
        swapChildren(scoreBoard,jelly);
        jellyRightCreated = true;
      }
        //create jelly from left side
      else if ((miliseconds == 2 || miliseconds == 3) && !jellyLeftCreated) 
      {
        speed = Math.random() + GameProperties.speedCo;
        size = myJelly.size * (Math.random()+Math.random());
        
        jelly = new Jelly(space, size, -speed);
        
        //set position
        jelly.body.position.x = GameProperties.stageWidth;
        jelly.body.position.y = GameUtilities.randomInt(0, GameProperties.stageHeight - jelly.height);
        
        createdJellies.push(jelly);
        addChild(jelly);
        swapChildren(scoreBoard,jelly);
        jellyLeftCreated = true;
      }
      else if(miliseconds > 2)
      {
        jellyRightCreated = false;
        jellyLeftCreated = false;
      }
    }
    
    private function createScoreBoard():void
    {
      scoreBoard = new TextField(500, 100, "0");
      
      scoreBoard.hAlign = HAlign.RIGHT;
      scoreBoard.vAlign = VAlign.TOP;
      
      scoreBoard.color = 0xC36767;
      
      scoreBoard.fontSize = 30;
      
      scoreBoard.x = GameProperties.scoreBoardX;
      scoreBoard.y = GameProperties.scoreBoardY;
      
      addChild(scoreBoard);
    }
    
    private function gameOver():void
    {
      Mouse.show();
      
      //arrange textfield
      scoreBoard.hAlign = HAlign.CENTER;
      scoreBoard.color = 0x000000;
      scoreBoard.fontSize = 80;
      scoreBoard.text = score.toString();
      
      //get images
      var overlay:Image = new Image(Assets.getAtlas().getTexture("gameover_overlay"));
      var playAgainButton:Button = new Button(Assets.getAtlas().getTexture("play_again_button"));
      var jellyTypesImage:Image = new Image(Assets.getAtlas().getTexture("jelly_menu_image"));
      var scoreTitle:Image = new Image(Assets.getAtlas().getTexture("score_title"));
      
      //set positions
      overlay.x = GameProperties.overlayX;
      overlay.y = GameProperties.overlayY;
      
      playAgainButton.x = (GameProperties.stageWidth - playAgainButton.width)/2;
      playAgainButton.y = GameProperties.playAgainButtonY;
      
      jellyTypesImage.x = (GameProperties.stageWidth - jellyTypesImage.width)/2;
      jellyTypesImage.y = GameProperties.menuJellyY;
      
      scoreTitle.x = (GameProperties.stageWidth - scoreTitle.width)/2;
      scoreTitle.y = GameProperties.scoreTitleY;
      
      scoreBoard.x = (GameProperties.stageWidth - scoreBoard.width)/2;
      scoreBoard.y = GameProperties.gameoverScoreY;
      
      overlay.alpha = 0.8;
      
      addChild(overlay);
      addChild(playAgainButton);
      addChild(scoreTitle);
      addChild(scoreBoard);
      
      playAgainButton.addEventListener(Event.TRIGGERED, onPlayAgainButton);
      
    }
    
    private function onPlayAgainButton():void
    {
        removeChildren();
        createdJellies = null;
        space.bodies.clear();
        setUp();
    }
  }
}