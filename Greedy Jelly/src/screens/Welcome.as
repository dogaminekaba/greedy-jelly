package screens
{
  import starling.display.Button;
  import starling.display.Image;
  import starling.display.Sprite;
  import starling.events.Event;
  
  import utilities.GameProperties;
  
  public class Welcome extends Sprite
  {
    private var bg:Image;
    private var gameTitle:Image;
    private var jellyImages:Image
    private var playBtn:Button;
    private var infoBtn:Button;
    private var screenInGame:InGame;
    
    public function Welcome()
    {
      super();
      this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
    }
    
    private function onAddedToStage():void
    {
      drawScreen();
    }
    
    private function drawScreen():void
    {
      bg = new Image(Assets.getAtlas().getTexture("bg"));
      this.addChild(bg);
      
      gameTitle = new Image(Assets.getAtlas().getTexture("game_title"));
      gameTitle.x = GameProperties.gameTitleX;
      gameTitle.y = GameProperties.gameTitleY;
      this.addChild(gameTitle);
      
      jellyImages = new Image(Assets.getAtlas().getTexture("jelly_menu_image"));
      jellyImages.x = GameProperties.jelliesImageX;
      jellyImages.y = GameProperties.jelliesImageY;
      this.addChild(jellyImages);
      
      playBtn = new Button(Assets.getAtlas().getTexture("play_button"));
      playBtn.x = GameProperties.playButtonX;
      playBtn.y = GameProperties.playButtonY;
      this.addChild(playBtn);
      
      infoBtn = new Button(Assets.getAtlas().getTexture("info_button"));
      infoBtn.x = GameProperties.infoButtonX;
      infoBtn.y = GameProperties.infoButtonY;
      this.addChild(infoBtn);
      
      playBtn.addEventListener(Event.TRIGGERED, onPlay);
    }
    
    private function onPlay():void
    {
      screenInGame = new InGame();
      parent.addChild(screenInGame);
      this.visible = false;
      this.dispose();
    }
  }
}